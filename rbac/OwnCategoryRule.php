<?php

//OwnCategoryRule

namespace app\rbac;

use yii\rbac\Rule;
use Yii; 

class OwnCategoryRule extends Rule
{
	public $name = 'OwnCategoryRule';
// item- ����� ����� ���� ����, Params-  ����� �� ������� ������� ������ ����� ����
	public function execute($user, $item, $params)
	{
		if (!Yii::$app->user->isGuest) {
			return isset($params['user']) ? $params['user']->id == $user : false;
		}
		return false;
	}
}
